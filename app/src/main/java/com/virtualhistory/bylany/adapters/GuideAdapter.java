package com.virtualhistory.bylany.adapters;

/**
 * Created by Rashida on 07/03/16.
 */

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.virtualhistory.bylany.EighthGuideActivity;
import com.virtualhistory.bylany.FifthGuideActivity;
import com.virtualhistory.bylany.FirstGuideActivity;
import com.virtualhistory.bylany.FourthGuideActivity;

import com.virtualhistory.bylany.NinthGuideActivity;

import com.virtualhistory.bylany.R;
import com.virtualhistory.bylany.SecondGuideActivity;
import com.virtualhistory.bylany.SeventhGuideActivity;
import com.virtualhistory.bylany.SixthGuideActivity;
import com.virtualhistory.bylany.ThirdGuideActivity;
import com.virtualhistory.bylany.bean.Guide;

/**
 * Created by huzefaasger on 04-01-2016.
 */

public class GuideAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Guide> entries;
    private final LayoutInflater inflater;
    private static Context mContext;

    private ImageLoadingListener imageLoadingListener;
    DisplayImageOptions options;
    ImageLoader imageLoader;
    int [] titles,desc;

    private static final int TYPE_HEADER = 2;
    private static final int TYPE_ITEM = 1;

    public GuideAdapter(Context context, List<Guide> entries) {
        this.entries = entries;
        inflater = LayoutInflater.from(context);
        mContext = context;
        titles = new int[]{
                R.string.guide_1_heading_1,
                R.string.guide_2_heading_1,
                R.string.guide_3_heading_1,
                R.string.guide_4_heading_1,
                R.string.guide_5_heading_1,
                R.string.guide_6_heading_1,
                R.string.guide_7_heading_1,
                R.string.guide_8_heading_1,
                R.string.guide_9_heading_1
        };
        desc = new int[]{
                R.string.guide_list_desc_1,
                R.string.guide_list_desc_2,
                R.string.guide_list_desc_3,
                R.string.guide_list_desc_4,
                R.string.guide_list_desc_5,
                R.string.guide_list_desc_6,
                R.string.guide_list_desc_7,
                R.string.guide_list_desc_8,
                R.string.guide_list_desc_9

        };
        imageLoadingListener = new ImageLoadingListener();
        imageLoader = ImageLoader.getInstance();
        initImageLoader(context, imageLoader);

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.loading_spinner)
                .showImageForEmptyUri(R.drawable.ic_empty)
                .showImageOnFail(R.drawable.ic_error)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .resetViewBeforeLoading(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .build();

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        ImageView icon;
        TextView title, description;
        RelativeLayout relativeLayout;

        public ViewHolder(View view) {
            super(view);
            icon = (ImageView) view.findViewById(R.id.icon);
            title = ((TextView) view.findViewById(R.id.title));
            description = (TextView) view.findViewById(R.id.description);
            relativeLayout = (RelativeLayout)view.findViewById(R.id.relative);
        }
    }

    public class RecyclerHeaderViewHolder extends RecyclerView.ViewHolder {
        public RecyclerHeaderViewHolder(View itemView) {
            super(itemView);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        }
        return TYPE_ITEM;
    }

    //added a method to check if given position is a header
    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        if (viewType == TYPE_ITEM) {
            View view = inflater.inflate(R.layout.guide_list_item, parent, false);
            ViewHolder vh = new ViewHolder(view);
            return vh;
        } else if (viewType == TYPE_HEADER) {
            final View view = LayoutInflater.from(context).inflate(R.layout.header, parent, false);
            return new RecyclerHeaderViewHolder(view);
        }
        throw new RuntimeException("There is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder v, int position) {

        if (!isPositionHeader(position)) {

            final Guide entry = entries.get(position - 1);
            ViewHolder view = (ViewHolder) v;
            view.title.setText(titles[position-1]);
            view.description.setText(desc[position-1]);
            String iconUrl = "drawable://"+entry.getImageUri();
            imageLoader.displayImage(iconUrl, view.icon, options, imageLoadingListener);
            view.relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent;
                    switch (entry.getId())
                    {
                        case 1:
                            intent = new Intent(mContext, FirstGuideActivity.class);
                            break;
                        case 2:
                            intent = new Intent(mContext, SecondGuideActivity.class);
                            break;
                        case 3:
                            intent = new Intent(mContext, ThirdGuideActivity.class);
                            break;
                        case 4:
                            intent = new Intent(mContext, FourthGuideActivity.class);
                            break;
                        case 5:
                            intent = new Intent(mContext, FifthGuideActivity.class);
                            break;
                        case 6:
                            intent = new Intent(mContext, SixthGuideActivity.class);
                            break;
                        case 7:
                            intent = new Intent(mContext, SeventhGuideActivity.class);
                            break;
                        case 8:
                            intent = new Intent(mContext, EighthGuideActivity.class);
                            break;
                        case 9:
                            intent = new Intent(mContext, NinthGuideActivity.class);
                            break;
                        default:
                            intent = new Intent(mContext, FirstGuideActivity.class);
                            break;

                    }
                    mContext.startActivity(intent);
                }
            });

        }
    }


    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return entries.size() + 1;
    }

 /*  @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        Guide entry = entries.get(position);
        ImageView thumbnail, videoPreviewPlayButton;
        // There are three cases here
        if (view == null) {
            // 1) The view has not yet been created - we need to initialize the YouTubeThumbnailView.
            view = inflater.inflate(R.layout.guide_list_item, parent, false);
            thumbnail = (ImageView) view.findViewById(R.id.imagethumbnail);

        } else {
            thumbnail = (ImageView) view.findViewById(R.id.imagethumbnail);

        }

        videoPreviewPlayButton = (ImageView) view.findViewById(R.id.videoPreviewPlayButton);

        thumbnail.getLayoutParams().height = height;
        thumbnail.getLayoutParams().width = width;
        thumbnail.requestLayout();

        videoPreviewPlayButton.getLayoutParams().height = height;
        videoPreviewPlayButton.getLayoutParams().width = width;
        videoPreviewPlayButton.requestLayout();

        TextView label = ((TextView) view.findViewById(R.id.text));
        final ImageView fbShare = (ImageView) view.findViewById(R.id.share_facebook);
        //ImageButton twitterShare = (ImageButton)view.findViewById(R.id.share_twitter);
        ImageView watsappShare = (ImageView) view.findViewById(R.id.share_watsapp);
        fbShare.setTag(entry.getVideoId());
        //  twitterShare.setTag(entry.getVideoId());

        watsappShare.setTag(entry.getVideoId() + ";" + entry.getPostTitle());

        thumbnail.setTag(entry.getVideoId());

        thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFullScreenVideo((String) v.getTag());
            }
        });


        fbShare.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ImageView view = (ImageView) v;
                        view.setColorFilter(Color.argb(255, 255, 255, 255)); // White Tint
                        shareOnFacebook((String) view.getTag());
                        v.invalidate();
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL: {
                        ImageView view2 = (ImageView) v;
                        //clear the overlay
                        view2.getDrawable().clearColorFilter();
                        view2.invalidate();
                        return true;
                    }
                }
                return false;
            }
        });


        watsappShare.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ImageView view = (ImageView) v;
                        view.setColorFilter(Color.argb(255, 255, 255, 255)); // White Tint
                        try {
                            String tags[] = ((String) view.getTag()).split(";");
                            shareOnWatsapp(tags[0], tags[1]);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        v.invalidate();
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL: {
                        ImageView view2 = (ImageView) v;
                        //clear the overlay
                        view2.getDrawable().clearColorFilter();
                        view2.invalidate();
                        return true;
                    }
                }
                return false;
            }
        });


        String youtubeTag = "http://img.youtube.com/vi/" + entry.getVideoId() + "/0.jpg";
        ImageSize targetSize = new ImageSize(width, height);
        imageLoader.displayImage(youtubeTag, thumbnail, options, imageLoadingListener);
        label.setText(entry.getPostTitle());
        label.setVisibility(View.VISIBLE);
        return view;
    }*/



    public static void initImageLoader(Context context, ImageLoader imageLoader) {
        // This configuration tuning is custom. You can tune every option, you may tune some of them,
        // or you can create default configuration by
        //  ImageLoaderConfiguration.createDefault(this);
        // method.
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .diskCacheSize(20 * 1024 * 1024) // 20 Mb
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .writeDebugLogs() // Remove for release app

                .build();
        // Initialize ImageLoader with configuration.
        imageLoader.init(config);
    }


    private static class ImageLoadingListener extends SimpleImageLoadingListener {

        static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);

                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }

        @Override
        public void onLoadingStarted(String imageUri, View view) {
            super.onLoadingStarted(imageUri, view);
        }
    }
}


