package com.virtualhistory.bylany.utility;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.virtualhistory.bylany.R;


/**
 * Created by huzefaasger on 08-03-2016.
 */
public class CustomTextViewLight extends TextView {

    public CustomTextViewLight(Context context) {
        super(context);

        applyCustomFont(context);
    }

    public CustomTextViewLight(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context);
    }

    public CustomTextViewLight(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface("fonts/RobotoSlab-Light.ttf", context);
        setTypeface(customFont);
        setTextColor(getResources().getColor(R.color.textcolor_regular));
    }
}