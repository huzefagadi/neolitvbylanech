package com.virtualhistory.bylany.utility;

/**
 * Created by huzefaasger on 08-03-2016.
 */

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.OverlayItem;

import java.util.List;

import com.virtualhistory.bylany.EighthGuideActivity;
import com.virtualhistory.bylany.FifthGuideActivity;
import com.virtualhistory.bylany.FirstGuideActivity;
import com.virtualhistory.bylany.FourthGuideActivity;
import com.virtualhistory.bylany.NinthGuideActivity;

import com.virtualhistory.bylany.R;
import com.virtualhistory.bylany.SecondGuideActivity;
import com.virtualhistory.bylany.SeventhGuideActivity;
import com.virtualhistory.bylany.SixthGuideActivity;
import com.virtualhistory.bylany.ThirdGuideActivity;


/**
 * Created by huzefaasger on 08-03-2016.
 */
public class CustomizedOverlay extends ItemizedIconOverlay<OverlayItem> {
    protected Context mContext;
    private MapView mapView;

    int [] titles = new int[]{
        R.string.guide_1_heading_1,
                R.string.guide_2_heading_1,
                R.string.guide_3_heading_1,
                R.string.guide_4_heading_1,
                R.string.guide_5_heading_1,
                R.string.guide_6_heading_1,
                R.string.guide_7_heading_1,
                R.string.guide_8_heading_1,
                R.string.guide_9_heading_1
    };
    int [] desc = new int[]{
        R.string.guide_list_desc_1,
                R.string.guide_list_desc_2,
                R.string.guide_list_desc_3,
                R.string.guide_list_desc_4,
                R.string.guide_list_desc_5,
                R.string.guide_list_desc_6,
                R.string.guide_list_desc_7,
                R.string.guide_list_desc_8,
                R.string.guide_list_desc_9

    };
    int [] icons = new int[]{
            R.drawable.icon_1,
            R.drawable.icon_2,
            R.drawable.icon_3,
            R.drawable.icon_4,
            R.drawable.icon_5,
            R.drawable.icon_6,
            R.drawable.icon_7,
            R.drawable.icon_8,
            R.drawable.icon_9,

    };
    public CustomizedOverlay(final Context context, MapView mapView,final List<OverlayItem> aList) {
        super(context, aList, new OnItemGestureListener<OverlayItem>() {
            @Override public boolean onItemSingleTapUp(final int index, final OverlayItem item) {
                return false;
            }
            @Override public boolean onItemLongPress(final int index, final OverlayItem item) {
                return false;
            }
        } );
        // TODO Auto-generated constructor stub
        mContext = context;
        this.mapView = mapView;
    }

    @Override
    protected boolean onSingleTapUpHelper(final int index, final OverlayItem item, final MapView mapView) {
        //Toast.makeText(mContext, "Item " + index + " has been tapped!", Toast.LENGTH_SHORT).show();


        Dialog dialog = new Dialog(mContext,R.style.AppTheme_NoActionBar);

        dialog.setContentView(R.layout.map_item);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        final int position = Integer.parseInt(item.getSnippet())-1;
        TextView text = (TextView) dialog.findViewById(R.id.title);
        text.setText(titles[position]);
        TextView text2 = (TextView) dialog.findViewById(R.id.description);
        text2.setText(desc[position]);
        ImageView image = (ImageView) dialog.findViewById(R.id.icon);
        image.setImageResource(icons[position]);
        RelativeLayout relativeLayout = (RelativeLayout) dialog.findViewById(R.id.relative);
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                mapView.getTileProvider().clearTileCache();
                switch (position+1)
                {
                    case 1:
                        intent = new Intent(mContext, FirstGuideActivity.class);
                        break;
                    case 2:
                        intent = new Intent(mContext, SecondGuideActivity.class);
                        break;
                    case 3:
                        intent = new Intent(mContext, ThirdGuideActivity.class);
                        break;
                    case 4:
                        intent = new Intent(mContext, FourthGuideActivity.class);
                        break;
                    case 5:
                        intent = new Intent(mContext, FifthGuideActivity.class);
                        break;
                    case 6:
                        intent = new Intent(mContext, SixthGuideActivity.class);
                        break;
                    case 7:
                        intent = new Intent(mContext, SeventhGuideActivity.class);
                        break;
                    case 8:
                        intent = new Intent(mContext, EighthGuideActivity.class);
                        break;
                    case 9:
                        intent = new Intent(mContext, NinthGuideActivity.class);
                        break;
                    default:
                        intent = new Intent(mContext, FirstGuideActivity.class);
                        break;

                }
                mContext.startActivity(intent);
            }
        });
        dialog.show();
        dialog.getWindow().setAttributes(lp);
        return true;
    }
}