package com.virtualhistory.bylany;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;

import org.osmdroid.api.IMapController;
import org.osmdroid.tileprovider.MapTileProviderArray;
import org.osmdroid.tileprovider.modules.MapTileAssetsProvider;
import org.osmdroid.tileprovider.modules.MapTileModuleProviderBase;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.tileprovider.util.SimpleRegisterReceiver;
import org.osmdroid.util.BoundingBoxE6;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import java.util.ArrayList;



import com.virtualhistory.bylany.utility.CustomizedOverlay;

public class MapsActivity extends Activity {

    MapView map;
    MyLocationNewOverlay mMyLocationNewOverlay;
    ArrayList<OverlayItem> overlayItemArray;
    ProgressDialog progressDialog;
    MainApplication mainApplication;
    private MapTileProviderArray mapTileProviderArray;
    private MapTileAssetsProvider prov;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_maps);
        map = (MapView) findViewById(R.id.mapview);

        progressDialog= new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("please wait..");
        progressDialog.show();
        //OpenStreetMapTileProviderConstants.DEBUGMODE=true;
        //OpenStreetMapTileProviderConstants.DEBUG_TILE_PROVIDERS=true;

        prov = new MapTileAssetsProvider(new SimpleRegisterReceiver(getApplicationContext()), getAssets());
        mapTileProviderArray = new MapTileProviderArray(TileSourceFactory.MAPQUESTOSM, new SimpleRegisterReceiver(getApplicationContext()), new MapTileModuleProviderBase[]{prov});
        map.setTileProvider(mapTileProviderArray);
        map.setTileSource(TileSourceFactory.MAPQUESTOSM);
        map.setUseDataConnection(false); //optional, but a good way to prevent loading from the network and test your zip loading.
        map.setMinZoomLevel(15);
        map.setBuiltInZoomControls(true);
        map.setMultiTouchControls(true);
        map.setClickable(true);

        mMyLocationNewOverlay = new MyLocationNewOverlay(MapsActivity.this, map);
        mMyLocationNewOverlay.setDrawAccuracyEnabled(true);
        mMyLocationNewOverlay.enableMyLocation();


        mMyLocationNewOverlay.setDrawAccuracyEnabled(true);

        mMyLocationNewOverlay.runOnFirstFix(new Runnable() {
            public void run() {
             //   map.getController().animateTo(mMyLocationNewOverlay.getMyLocation());
            }
        });
        map.getOverlays().add(mMyLocationNewOverlay);
        Drawable newMarker = this.getResources().getDrawable(R.drawable.marker);


        overlayItemArray = new ArrayList<OverlayItem>();

        OverlayItem linkopingItem1 = new OverlayItem("one", "1",
                new GeoPoint(49.943996, 15.220004));
        OverlayItem linkopingItem2 = new OverlayItem("two", "2",
                new GeoPoint(49.939466, 15.226317));
        OverlayItem linkopingItem3 = new OverlayItem("three", "3",
                new GeoPoint(49.936145, 15.239977));
        OverlayItem linkopingItem4 = new OverlayItem("four", "4",
                new GeoPoint(49.935632, 15.240467));
        OverlayItem linkopingItem5 = new OverlayItem("five", "5",
                new GeoPoint(49.936465, 15.242001));
        OverlayItem linkopingItem6 = new OverlayItem("six", "6",
                new GeoPoint(49.93659, 15.245802));
        OverlayItem linkopingItem7 = new OverlayItem("seven", "7",
                new GeoPoint(49.931622, 15.246505));
        OverlayItem linkopingItem8 = new OverlayItem("Eight", "8",
                new GeoPoint(49.928276, 15.246604));
        OverlayItem linkopingItem9 = new OverlayItem("Nine", "9",
                new GeoPoint(49.929521, 15.252481));


        linkopingItem1.setMarker(newMarker);
        linkopingItem2.setMarker(newMarker);
        linkopingItem3.setMarker(newMarker);
        linkopingItem4.setMarker(newMarker);
        linkopingItem5.setMarker(newMarker);
        linkopingItem6.setMarker(newMarker);
        linkopingItem7.setMarker(newMarker);
        linkopingItem8.setMarker(newMarker);
        linkopingItem9.setMarker(newMarker);


        overlayItemArray.add(linkopingItem1);
        overlayItemArray.add(linkopingItem2);
        overlayItemArray.add(linkopingItem3);
        overlayItemArray.add(linkopingItem4);
        overlayItemArray.add(linkopingItem5);
        overlayItemArray.add(linkopingItem6);
        overlayItemArray.add(linkopingItem7);
        overlayItemArray.add(linkopingItem8);
        overlayItemArray.add(linkopingItem9);
        final IMapController mapController = map.getController();
        mapController.setCenter(new GeoPoint(49.936465, 15.242001));
        mapController.animateTo(new GeoPoint(49.936465, 15.242001));
        mapController.setZoom(15);
        CustomizedOverlay overlay = new CustomizedOverlay(this,map,overlayItemArray);
        map.getOverlays().add(overlay);

        new Handler().postDelayed(new Runnable(){
            public void run() {
                BoundingBoxE6 box = map.getBoundingBox();
                map.setScrollableAreaLimit(box);
                progressDialog.dismiss();
            }}, 2000);

        //map.setScrollableAreaLimit(box);
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (mMyLocationNewOverlay != null) {
            mMyLocationNewOverlay.disableMyLocation();
        }
        map.getTileProvider().clearTileCache();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mMyLocationNewOverlay != null) {
            mMyLocationNewOverlay.enableMyLocation();
        }

    }
}
