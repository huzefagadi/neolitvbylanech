package com.virtualhistory.bylany;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;



import com.virtualhistory.bylany.utility.CustomTextViewBold;
import com.virtualhistory.bylany.utility.FontCache;

public class MainActivity extends Activity {

    Button information, guide, maps;
    ImageView iconFor3d;
    CustomTextViewBold textViewBold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);
        textViewBold = (CustomTextViewBold) findViewById(R.id.textView);
        textViewBold.setTextColor(getResources().getColor(R.color.background_color));
        Typeface customFont = FontCache.getTypeface("fonts/RobotoSlab-Bold.ttf", this);
        information = (Button) findViewById(R.id.information);
        information.setTypeface(customFont);
        information.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, InformationActivity.class);
                startActivity(intent);
            }
        });

        guide = (Button) findViewById(R.id.pruvode);
        guide.setTypeface(customFont);
        guide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, GuideActivity.class);
                startActivity(intent);
            }
        });

        maps = (Button) findViewById(R.id.maps);
        maps.setTypeface(customFont);
        maps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MapsActivity.class);
                startActivity(intent);
            }
        });

        iconFor3d = (ImageView) findViewById(R.id.icon_top);
        iconFor3d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "http://www.archaeo3d.com";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
    }


}
