package com.virtualhistory.bylany;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;



import com.virtualhistory.bylany.adapters.GuideAdapter;
import com.virtualhistory.bylany.bean.Guide;

public class GuideActivity extends Activity {

    RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.guide_list);
        int [] icons = new int[]{R.drawable.icon_1,
                R.drawable.icon_2,
                R.drawable.icon_3,
                R.drawable.icon_4,
                R.drawable.icon_5,
                R.drawable.icon_6,
                R.drawable.icon_7,
                R.drawable.icon_8,
                R.drawable.icon_9,};


        recyclerView = (RecyclerView) findViewById(R.id.list_fragment);
        List<Guide> listOfGuides = new ArrayList<>();
        for(int i=1;i<10;i++)
        {
            Guide guide = new Guide();
            guide.setId(i);
            guide.setDescription("test "+i);
            guide.setTitle("description " + i);
            guide.setImageUri(icons[i-1]);
            listOfGuides.add(guide);

        }


        LinearLayoutManager mLayoutManager = new LinearLayoutManager(GuideActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
        GuideAdapter adapter = new GuideAdapter(this,listOfGuides);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        recyclerView.requestLayout();


    }

}
