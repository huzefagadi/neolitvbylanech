package com.virtualhistory.bylany;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;


import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.virtualhistory.bylany.panorama.PanoramaActivity;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class ThirdGuideActivity extends Activity {

    ImageView view360;
    private ImageLoadingListener imageLoadingListener;
    DisplayImageOptions options;
    ImageLoader imageLoader;
    ImageView image1,image2,image3,image4,image5,image6;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_third_guide);

        image1 = (ImageView) findViewById(R.id.imageView);
        image2 = (ImageView) findViewById(R.id.imageView2);
        image3 = (ImageView) findViewById(R.id.imageView3);
        image4 = (ImageView) findViewById(R.id.imageView4);
        image5 = (ImageView) findViewById(R.id.imageView5);
        image6 = (ImageView) findViewById(R.id.imageView6);
        view360 = (ImageView) findViewById(R.id.image_360);
        view360.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ThirdGuideActivity.this, PanoramaActivity.class);
                intent.putExtra("imageurl", R.raw.guide_360_3);
                startActivity(intent);
            }
        });
        imageLoadingListener = new ImageLoadingListener();
        imageLoader = ImageLoader.getInstance();
        initImageLoader(this, imageLoader);

        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.loading_spinner)
                .showImageForEmptyUri(R.drawable.ic_empty)
                .showImageOnFail(R.drawable.ic_error)
                .cacheInMemory(false)
                .cacheOnDisk(false)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .resetViewBeforeLoading(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .build();

        String iconUrl1 = "drawable://"+R.drawable.guide_3_1;
        String iconUrl2 = "drawable://"+R.drawable.guide_3_2;
        String iconUrl3 = "drawable://"+R.drawable.guide_3_3;
        String iconUrl4 = "drawable://"+R.drawable.guide_3_4;
        String iconUrl5 = "drawable://"+R.drawable.guide_3_5;
        String iconUrl6 = "drawable://"+R.drawable.guide_3_6;
        imageLoader.displayImage(iconUrl1, image1, options, imageLoadingListener);
        imageLoader.displayImage(iconUrl2, image2, options, imageLoadingListener);
        imageLoader.displayImage(iconUrl3, image3, options, imageLoadingListener);
        imageLoader.displayImage(iconUrl4, image4, options, imageLoadingListener);
        imageLoader.displayImage(iconUrl5, image5, options, imageLoadingListener);
        imageLoader.displayImage(iconUrl6, image6, options, imageLoadingListener);

    }

    public static void initImageLoader(Context context, ImageLoader imageLoader) {
        // This configuration tuning is custom. You can tune every option, you may tune some of them,
        // or you can create default configuration by
        //  ImageLoaderConfiguration.createDefault(this);
        // method.
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .diskCacheSize(20 * 1024 * 1024) // 20 Mb
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .writeDebugLogs() // Remove for release app

                .build();
        // Initialize ImageLoader with configuration.
        imageLoader.init(config);
    }


    private static class ImageLoadingListener extends SimpleImageLoadingListener {

        static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);

                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }

        @Override
        public void onLoadingStarted(String imageUri, View view) {
            super.onLoadingStarted(imageUri, view);
        }
    }

}
