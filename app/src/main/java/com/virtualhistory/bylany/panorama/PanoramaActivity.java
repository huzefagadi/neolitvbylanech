package com.virtualhistory.bylany.panorama;

import android.app.Activity;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.panorama.Panorama;
import com.google.android.gms.panorama.PanoramaApi;

/**
 * Created by Rashida on 07/03/16.
*/

import android.content.Intent;
import android.net.Uri;
import android.util.Log;

/**
 * Displays examples of integrating with the panorama viewer API.
 */
public class PanoramaActivity extends Activity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    public static final String TAG = PanoramaActivity.class.getSimpleName();

    private GoogleApiClient mClient;

    int code;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        code = getIntent().getIntExtra("imageurl",0);
        mClient = new GoogleApiClient.Builder(this, this, this)
                .addApi(Panorama.API)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();
        if(!mClient.isConnected())
        mClient.connect();
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        if (code!=0)
        {
            Uri uri = Uri.parse("android.resource://" + getPackageName() + "/" + code);
            Panorama.PanoramaApi.loadPanoramaInfo(mClient, uri).setResultCallback(
                    new ResultCallback<PanoramaApi.PanoramaResult>() {
                        @Override
                        public void onResult(PanoramaApi.PanoramaResult result) {
                            if (result.getStatus().isSuccess()) {
                               Intent viewerIntent = result.getViewerIntent();
                               // Intent viewerIntent = new Intent("application/vnd.google.panorama360+jpg");

                                Log.i(TAG, "found viewerIntent: " + viewerIntent);
                                if (viewerIntent != null) {
                                    startActivityForResult(viewerIntent, 0);
                                }
                            } else {
                                Log.e(TAG, "error: " + result);
                            }
                        }
                    });
        }

    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.i(TAG, "connection suspended: " + cause);
    }

    @Override
    public void onConnectionFailed(ConnectionResult status) {
        Log.e(TAG, "connection failed: " + status);
        // TODO fill in
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 0)
        {
            finish();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        mClient.disconnect();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
